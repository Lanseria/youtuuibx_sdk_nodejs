const libpath = './youtu_lib';

module.exports = {
  auth: require(`${libpath}/auth.js`),
  conf: require(`${libpath}/conf.js`),
  youtu: require(`${libpath}/youtu.js`)
};