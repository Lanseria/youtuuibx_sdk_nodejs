const http = require('http');
const https = require('https');
const fs = require('fs');
const got = require('got');

const auth = require('./auth');
const conf = require('./conf');

// 30 days
const EXPIRED_SECONDS = 2592000;

/**
 * return the status message
 */
function statusText(status) {
  let statusText = 'unknow';
  switch (status) {
    case 200:
      statusText = 'HTTP OK';
      break;
    case 400:
      statusText = 'Bad Request';
      break;
    case 401:
      statusText = 'Unauthorized';
      break;
    case 403:
      statusText = 'Forbidden';
      break;
    case 500:
      statusText = 'Internal Server Error';
    default:
      break;
  }
  return statusText;
}

function getRequest(protocol, params, callback) {
  return protocol.request(params, (response) => {
    if (response.statusCode != 200) {
      callback({
        httpcode: response.statusCode,
        code: response.statusCode,
        message: statusText(response.statusCode),
        data: {}
      });
      return;
    }
    let body = '';
    response.setEncoding('utf8');
    response.on('data', (chunk) => {
      body += chunk;
    })
    response.on('end', () => {
      callback({
        httpcode: response.statusCode,
        code: response.statusCode,
        message: statusText(response.statusCode),
        data: JSON.parse(body)
      });
    })
    response.on('error', (e) => {
      callback({
        httpcode: response.statusCode,
        code: response.statusCode,
        message: '' + e,
        data: {}
      });
    })
  });
}

const nullResponseCallData = (message) => {
  return {
    httpcode: 0,
    code: -1,
    message,
    data: {}
  }
}

/**
 * @brief detectface
 * @param imagePath 待检测的路径（本地路径或url）
 * @param isbigface 是否大脸模式 0 表示检测所有人脸，1 表示只检测照片最大人脸适合单人照模式
 * @param callback 回调函数, 参见 Readme 文档
 */
exports.detectface = function (imagePath, isbigface, callback) {
  callback = callback || function (ret) {
    console.log(ret)
  };
  const expired = parseInt(Date.now() / 1000) + EXPIRED_SECONDS;
  const sign = auth.appSign(expired);
  const tag = imagePath.substring(0, 4);
  let request_body = '';
  if (tag == 'http') {
    request_body = JSON.stringify({ // 会变
      app_id: conf.APPID,
      url: imagePath,
      mode: isbigface
    });
  } else {
    try {
      const data = fs.readFileSync(imagePath).toString('base64');
    } catch (err) {
      callback(nullResponseCallData(`file ${imagePath} not exits`));
      return;
    }
    if (data == null) {
      callback(nullResponseCallData(`file ${imagePath} not exits`));
      return;
    }
    request_body = JSON.stringify({ // 会变
      app_id: conf.APPID,
      image: data.toString('base64'),
      mode: isbigface
    });
  }
  const params = {
    hostname: conf.API_YOUTU_SERVER,
    path: '/youtu/api/detectface', // API 会变
    method: 'POST',
    headers: {
      'Authorization': sign,
      'User-Agent': conf.USER_AGENT(),
      'Content-Length': request_body.length,
      'Content-Type': 'text/json'
    }
  }
  let request = null;
  if (conf.API_DOMAIN == 0) {
    request = getRequest(http, params, callback);
  } else {
    request = getRequest(https, params, callback);
  }
  request.on('error', (err) => {
    callback(nullResponseCallData(err.message))
  })
  request.end(request_body);
}
/**
 * @ 人脸融合 此SDK需要预先上传模板，或者使用已有的模板
 * @param {*} imagePath 待检测的路径
 * @param {*} callback 回调函数
 */
exports.facefusion = function (imagePath, callback) {
  callback = callback || function (ret) {
    console.log(ret)
  };
  const expired = parseInt(Date.now() / 1000) + EXPIRED_SECONDS;
  const sign = auth.appSign(expired);
  const tag = imagePath.substring(0, 4);
  let request_body = '';
  if (tag == 'http') {
    request_body = JSON.stringify({
      app_id: conf.APPID,
      rsp_img_type: 'base64',
      url: imagePath,
      opdata: [{ //注意opdata是一个数组
        cmd: "doFaceMerge",
        params: {
          model_id: "youtu_76175_20180630203735_1628" // 通用模板id
        }
      }]
    });
  } else {
    try {
      const image_data = fs.readFileSync(imagePath).toString('base64');
    } catch (e) {
      callback(nullResponseCallData(`file ${imagePath} not exits`));
      return;
    }
    if (image_data == null) {
      callback(nullResponseCallData(`file ${imagePath} not exits`));
      return;
    };
    request_body = JSON.stringify({
      app_id: conf.APPID,
      rsp_img_type: 'url',
      img_data: image_data.toString('base64'),
      opdata: [{ //注意opdata是一个数组
        cmd: "doFaceMerge",
        params: {
          model_id: "youtu_76175_20180630203735_1628" // 通用模板id
        }
      }]
    });
  }
  var params = {
    hostname: conf.API_YOUTU_SERVER,
    path: '/cgi-bin/pitu_open_access_for_youtu.fcg',
    method: 'POST',
    headers: {
      'Authorization': sign,
      'User-Agent': conf.USER_AGENT(),
      'Content-Length': request_body.length,
      'Content-Type': 'text/json'
    }
  };
  let request = null;
  if (conf.API_DOMAIN == 0) {
    request = getRequest(http, params, callback);
  } else {
    request = getRequest(https, params, callback);
  }
  request.on('error', function (e) {
    callback(nullResponseCallData(err.message));
  });
  request.end(request_body);
}

async function commonRequest(apiPath, request_body) {
  const expired = parseInt(Date.now() / 1000) + EXPIRED_SECONDS;
  const sign = auth.appSign(expired);
  const request = got(apiPath, {
    method: 'POST',
    hostname: conf.API_YOUTU_SERVER,
    headers: {
      'Authorization': sign,
      'User-Agent': conf.USER_AGENT(),
      'Content-Length': request_body.length,
      'Content-Type': 'text/json'
    },
    protocol: conf.API_DOMAIN == 0 ? 'http:' : 'https:',
    baseUrl: conf.API_YOUTU_SERVER,
    body: request_body
  })
  let response = ''
  try {
    response = await request;
  } catch (error) {
    console.log(error)
    return nullResponseCallData(error.message);
  }
  return response.body;
}

exports.detectfaceNew = async (imagePath, isbigface) => {
  const tag = imagePath.substring(0, 4);
  const data = '';
  let request_body = '';
  if (tag == 'http') {
    request_body = JSON.stringify({ // 会变
      app_id: conf.APPID,
      url: imagePath,
      mode: isbigface
    });
  } else {
    try {
      data = fs.readFileSync(imagePath).toString('base64');
    } catch (err) {
      return nullResponseCallData(`file ${imagePath} not exits`);
    }
    if (data == null) {
      return nullResponseCallData(`file ${imagePath} not exits`);
    }
    request_body = JSON.stringify({ // 会变
      app_id: conf.APPID,
      image: data.toString('base64'),
      mode: isbigface
    });
  }
  return await commonRequest('/youtu/api/detectface', request_body);
}